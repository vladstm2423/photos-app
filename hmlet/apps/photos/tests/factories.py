from django.contrib.auth import get_user_model

import factory

from photos.models import Photo

User = get_user_model()


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User


class PhotoFactory(factory.DjangoModelFactory):
    original_image = factory.django.ImageField(color='green')
    image = factory.django.ImageField(color='red')
    caption = factory.Sequence(lambda n: 'caption%d' % n)
    author = factory.SubFactory(UserFactory)

    class Meta:
        model = Photo

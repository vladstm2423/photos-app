import io

from django.contrib.auth import get_user_model
from django.urls import reverse

from PIL import Image
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from .factories import PhotoFactory, UserFactory

User = get_user_model()


class PhotoTestCase(APITestCase):

    def get_access_token_for_user(self, user):
        refresh = RefreshToken.for_user(user)
        return str(refresh.access_token)

    def setUp(self):
        self.user = User.objects.create(
            first_name="Test",
            last_name="Name",
            username="2123@gmasi.com",
            email="2123@gmasi.com",
            is_active=True,
            is_staff=False)
        self.user.set_password('demo1234')
        self.user.save()
        self.token = self.get_access_token_for_user(self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def generate_photo_file(self):
        file = io.BytesIO()
        image = Image.new('RGBA', size=(100, 100), color=(155, 0, 0))
        image.save(file, 'png')
        file.name = 'test.png'
        file.seek(0)
        return file

    def test_photos_create(self):
        url = reverse('photos-create')

        photo_file = self.generate_photo_file()

        data = {
            'original_image': photo_file,
            'caption': 'test caption'
        }

        response = self.client.post(url, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_photos_all(self):
        url = reverse('photos-list')

        photo_obj = PhotoFactory(author=self.user, draft=False)
        PhotoFactory()

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0]['caption'], photo_obj.caption)

    def test_photos_my(self):
        url = reverse('my-photos')

        photo_obj = PhotoFactory(author=self.user, draft=False)
        photo_obj_draft = PhotoFactory(author=self.user)

        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['caption'], photo_obj.caption)

        response = self.client.get(url + '?draft=true')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['caption'], photo_obj_draft.caption)

    def test_post_photo(self):
        photo_obj = PhotoFactory(author=self.user)
        photo_obj_denied = PhotoFactory(author=UserFactory())

        url = reverse('post-photo', kwargs={'photo_id': photo_obj.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        photo_obj.refresh_from_db()
        self.assertFalse(photo_obj.draft)

        url = reverse('post-photo', kwargs={'photo_id': photo_obj_denied.id})
        response = self.client.post(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        photo_obj.refresh_from_db()
        self.assertTrue(photo_obj_denied.draft)
        self.assertFalse(photo_obj.draft)

    def test_update_caption(self):
        photo_obj = PhotoFactory(author=self.user, draft=False, caption='1')
        url = reverse('photo-update-delete', kwargs={'pk': photo_obj.id})
        response = self.client.patch(url, {'caption': '2'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        photo_obj.refresh_from_db()
        self.assertEqual(photo_obj.caption, '2')

    def test_delete_photo(self):
        photo_obj = PhotoFactory(author=self.user, draft=False, caption='1')
        url = reverse('photo-update-delete', kwargs={'pk': photo_obj.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

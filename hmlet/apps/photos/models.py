from django.db import models
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class Photo(models.Model):
    original_image = models.ImageField(_('Original image file'), upload_to='original_images')
    image = models.ImageField(_('Cropped image'), upload_to='cropped_images')
    caption = models.CharField(_('Caption'), max_length=255, blank=True, null=True)
    draft = models.BooleanField(_('Draft'), default=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date_created = models.DateTimeField(_('Date created'), auto_now_add=True)
    date_updated = models.DateTimeField(_('Date created'), auto_now=True)

    class Meta:
        ordering = ('-date_created',)
        verbose_name = _('Photo')
        verbose_name_plural = _('Photos')

from django.urls import path

from .views import PhotoCreateAPIView, PhotoListAPIView, MyPhotosListAPIView, PhotosUpdateDeleteAPIView, \
    PostPhotoAPIView


urlpatterns = [
    path('', PhotoCreateAPIView.as_view(), name='photos-create'),
    path('all/', PhotoListAPIView.as_view(), name='photos-list'),
    path('my/', MyPhotosListAPIView.as_view(), name='my-photos'),
    path('<int:photo_id>/post/', PostPhotoAPIView.as_view(), name='post-photo'),
    path('<int:pk>/', PhotosUpdateDeleteAPIView.as_view(), name='photo-update-delete')
]

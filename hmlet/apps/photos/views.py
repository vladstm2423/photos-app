from django_filters.rest_framework import DjangoFilterBackend, filters
from rest_framework import permissions, status
from rest_framework.generics import CreateAPIView, ListAPIView, UpdateAPIView, DestroyAPIView, GenericAPIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .serializers import PhotoCreateSerializer, PhotoSerializer, PhotoUpdateSerializer
from .models import Photo


class IsOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.author == request.user


class OrderingMixin:
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['date_created']


class PhotoCreateAPIView(CreateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = PhotoCreateSerializer
    queryset = Photo.objects.all()
    parser_classes = [MultiPartParser, FormParser]

    def perform_create(self, serializer):
        serializer.validated_data.update({
            'author': self.request.user,
            'image': serializer.validated_data.get('original_image')
        })
        serializer.save()


class PhotoListAPIView(ListAPIView, OrderingMixin):
    serializer_class = PhotoSerializer
    queryset = Photo.objects.filter(draft=False)
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['author']


class MyPhotosListAPIView(ListAPIView, OrderingMixin):
    serializer_class = PhotoSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = Photo.objects.filter(author=self.request.user)
        if self.request.GET.get('draft'):
            queryset = queryset.filter(draft=True)
        else:
            queryset = queryset.filter(draft=False)
        return queryset


class PhotosUpdateDeleteAPIView(UpdateAPIView, DestroyAPIView):
    permission_classes = [IsAuthenticated, IsOwner]
    serializer_class = PhotoUpdateSerializer
    queryset = Photo.objects.all()


class PostPhotoAPIView(GenericAPIView):
    permission_classes = [IsAuthenticated, IsOwner]
    queryset = Photo.objects.all()

    def post(self, request, photo_id, *args, **kwargs):
        photo_obj = Photo.objects.filter(id=photo_id).first()
        if not photo_obj:
            return Response(status=status.HTTP_404_NOT_FOUND)
        self.check_object_permissions(request, photo_obj)
        photo_obj.draft = False
        photo_obj.save()
        return Response(status=status.HTTP_200_OK)

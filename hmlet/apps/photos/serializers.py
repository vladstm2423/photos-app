from rest_framework import serializers

from .models import Photo


class PhotoCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Photo
        fields = ('caption', 'original_image')


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ('caption', 'image')


class PhotoUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ('caption',)
